#!/bin/bash -eux

apt-get update
apt-get -y install xfce4 xrdp
update-rc.d xrdp defaults
apt-get clean all
apt-get autoclean all

cd /home/vagrant
tar -zxvf /tmp/xfce4-default_conf.tar.gz
rm -rf /tmp/xfce4-default_conf.tar.gz
