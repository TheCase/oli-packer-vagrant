OLI Ubuntu 14.04 LTS image builder
==

####Description

This will build a fresh Virtualbox image for use in Vagrant.

Requirements:

 * Packer      http://www.packer.io/
 * Vagrant     http://www.vagrantup.com/
 * Virtualbox  http://www.virtualbox.org/

####Instructions

Build box image

```
cd packer
packer build OLI_ubuntu-14.04-amd64.json
```
This will take a *very long time*



Add box to Vagrant

```
vagrant box add OLI/trusty64 OLI-ubuntu-14.04-provisionerless.box
```

This is configured to provision 12 boxes. To stand up a single host (or power back on an existing image):

```
cd ../vagrant
vagrant up xrdp01
```

You can add more hosts to the line if you wish to bring up more

```
vagrant up xrdp02 xrdp03 xrdp04
```

Power off host(s):

```
vagrant halt xrdp01
```

Trash a host (so you can bring up a fresh image):

```
vagrant destroy xrdp07
```

You can also ssh directly to a host if you whish:

```
vagrant ssh xrdp01
```
